$(function () {
 //Burger meni
    $('.hamburger-menu').on('click', function () {
        $('.toggle').toggleClass('open');
        $('.nav-list').toggleClass('open');
    });
//Animacije
    AOS.init({
        easing: 'ease',
        duration: 1000,
    });

    fadeStandfirst();
});
//Rollanjje kockica
$('.js-roller-btn').on('click', function(){
    var $this = $(this);
    var diceId = $this.data('id');
    var result = Math.ceil(Math.random()*diceId);
    $('#'+diceId).text('Rezultat:' + result);
});

function fadeStandfirst() {
    var elem = document.getElementById("hero-standfirst-animate");
    var opacity = 0;
    var id = setInterval(frame, 20);
    function frame() {
        if (opacity == 100)
        {
            clearInterval(id);
        }
        else
        {
            opacity++;
            elem.style.opacity = opacity + '%';
        }
    }
}